package com.rootproject.presenter;

public interface SampleConnector {

    interface View{

        void showResult(Object object);
        void fail(Object object);
    }

    interface SamplePresenter{
        void callApi();
    }
}
