package com.rootproject.utils.network;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitHandler<T>{


    public void request(Call<T> call,final int requestId,final boolean progressBar,final SuccessHandler<T> successHandler,final FailureHandler<T> failureHandler){

        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if(response.isSuccessful()){
                    successHandler.success(requestId,response.body());
                }else{
                    failureHandler.failure(requestId,"");
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                failureHandler.failure(requestId,"");
            }
        });
    }
}
