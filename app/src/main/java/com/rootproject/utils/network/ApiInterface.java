package com.rootproject.utils.network;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("/api/unknown")
    Call<Object> doGetListResources();
}
