package com.rootproject.utils.network;

public interface FailureHandler<T> {

    void failure(int requestId, String message);
}
