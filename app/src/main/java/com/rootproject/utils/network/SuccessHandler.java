package com.rootproject.utils.network;

public interface SuccessHandler<T> {

    void success(int requestId, T response);
}
