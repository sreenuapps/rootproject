package com.rootproject.view.activities;

import android.widget.Toast;

import com.rootproject.R;
import com.rootproject.base.BaseActivity;
import com.rootproject.databinding.ActivityMainBinding;
import com.rootproject.presenter.SampleConnector;
import com.rootproject.utils.network.APIClient;
import com.rootproject.utils.network.ApiInterface;
import com.rootproject.utils.network.FailureHandler;
import com.rootproject.utils.network.RetrofitHandler;
import com.rootproject.utils.network.SuccessHandler;
import com.rootproject.view.adapters.GenericRecyclerAdapter;

import java.util.ArrayList;

import retrofit2.Call;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements SampleConnector.View {


    @Override
    public int setLayoutId() {
        return R.layout.activity_main;
    }
    @Override
    public void init() {

        // ApiInterface apiInterface = APIClient.getClient().create(ApiInterface.class);
        // Call<Object> objectCall=apiInterface.doGetListResources();

        //RetrofitHandler retrofitHandler=new RetrofitHandler();
        // retrofitHandler.request(objectCall,100,true,this,this);



        //call generic adapter

        binding.recyclerView.setAdapter(new GenericRecyclerAdapter<String,ActivityMainBinding>(getApplicationContext(),new ArrayList()) {
            @Override
            public int getLayoutResId() {
                return R.layout.activity_main;
            }

            @Override
            public void onBindData(String model, int position, ActivityMainBinding dataBinding) {
                //dataBinding.txtName.setText("String " + position);
            }

            @Override
            public void onItemClick(String model, int position) {

            }
        });



    }


    @Override
    public void showResult(Object object) {

    }

    @Override
    public void fail(Object object) {

    }
}
